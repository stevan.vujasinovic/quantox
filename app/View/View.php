<?php
/**
 * Created by PhpStorm.
 * User: stevan
 * Date: 6.7.18.
 * Time: 14.24
 */

namespace App\View;


class View
{
    /**
     * @param string $view
     * @param array|null $data
     */
    public static function make($view, $data = null) {

        if ( ! is_null($data) and is_array($data)) {

            foreach ($data as $key => $val) {

                $$key = $val;

            }

        }

        include(dirname(__FILE__) . '/../../views/' . $view . '.php');

    }
}