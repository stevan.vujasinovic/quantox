<?php
/**
 * Created by PhpStorm.
 * User: stevan
 * Date: 5.7.18.
 * Time: 20.00
 */

namespace App\Controller;

use Core\Http\Request;
use App\Services\EventsCounterService;
use App\View\View;
use App\Model\Countries;
use App\Model\Events;

class EventCounterController
{

    /**
     *
     */
    public function index()
    {

        View::make('start');
    }

    /**
     *
     */
    public function eventPage()
    {
        $countries = new Countries();
        $countries_data = $countries->all();

        $events = new Events();
        $events_data = $events->all();
        View::make('event_page', ['form_data' => ['countries' => $countries_data, 'events' => $events_data]]);
    }
    /**
     * @param Request $request
     */
    public function addEventCounter(Request $request)
    {
        $data = json_decode($request->getField('data'), true);

        $country = $data['country'];
        $event = $data['event'];

        $events_counter_service = new EventsCounterService();
        echo $events_counter_service->storeEventsCounter($event, $country);


    }

    /**
     * @param Request $request
     */
    public function getData(Request $request)
    {
        $format = $request->getField('format');
        $events_counter_service = new EventsCounterService();
        $result = $events_counter_service->getEventsCounterData($format);

        View::make('show_statistic', ['statistic' => $result]);
    }
}