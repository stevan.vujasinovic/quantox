<?php
/**
 * Created by PhpStorm.
 * User: stevan
 * Date: 6.7.18.
 * Time: 10.30
 */

namespace App\Services;

use App\Model\EventsCounter;

use App\Helper\DataFormatHelper;

class EventsCounterService
{
    /**
     * @param $event_id
     * @param $country_id
     * @return string
     */
    public function storeEventsCounter($event_id, $country_id)
    {
        $date = date('Y-m-d');

        $events_counter = new EventsCounter();

        $events_counter->country_id = $country_id;
        $events_counter->event_id = $event_id;
        $events_counter->date = $date;
        $events_counter->counter = 1;

        return $events_counter->saveOrUpdate();

    }

    /**
     * @param $format
     * @return mixed|string
     */
    public function getEventsCounterData($format)
    {
        $events_counter = new EventsCounter();

        $sql = 'SELECT ev.event_name, ct.country_code, sum(ev_ct.counter) as counter
            FROM events_counter AS ev_ct
            INNER JOIN (
            SELECT id, country_id, SUM(counter) AS counter
            FROM events_counter
            GROUP BY country_id
            ORDER BY counter DESC limit 5) AS ev_ct1 ON ev_ct.country_id=ev_ct1.country_id
            INNER JOIN events as ev on ev.id=ev_ct.event_id
            INNER JOIN countries as ct on ct.id=ev_ct.country_id
            WHERE ev_ct.date >= DATE(NOW()) - INTERVAL 7 DAY
            GROUP BY ev_ct.event_id, ev_ct.country_id;';
        $data = $events_counter->raw($sql);

        if ($format == 'json') {
            return json_encode($data);
        } elseif ($format == 'xml') {
            $xml = new \SimpleXMLElement('<?xml version="1.0"?><EventsCounter></EventsCounter>');
            DataFormatHelper::toXml($xml, $data);
            header("Content-type: text/xml; charset=utf-8");
            return $xml->asXML();
        } else {
            return DataFormatHelper::toCsv($data);
        }
    }


}