<?php


spl_autoload_register(function ($class_name) {

    $base_path = __DIR__ . '/../';
    $class_name = str_replace('\\', '/', lcfirst($class_name));

    $file = $base_path . $class_name . '.php';
    if (file_exists($file)) {
        require $file;
    } else {
        exit('Mising class ' . $class_name);
    }

});
