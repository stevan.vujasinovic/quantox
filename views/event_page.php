<?php include __DIR__ . '/header.php'; ?>

<form action="index.php?start_counter" method="POST" id="event_counter_form">
    <label  for="countries">Chose country</label>
    <select id="countries" name="country">
        <option></option>

        <?php foreach ($form_data['countries'] as $country): ?>
            <option value="<?= $country['id']; ?>"><?= $country['country_code'];?></option>
        <?php endforeach; ?>
    </select><br/>
    <label  for="countries">Chose event</label>
    <select id="events" name="event">
        <option></option>

        <?php foreach ($form_data['events'] as $country): ?>
            <option value="<?= $country['id']; ?>"><?= $country['event_name'];?></option>
        <?php endforeach; ?>
    </select><br/><br/>
    <input type="submit" value="Confirm"/>
</form>
<?php include __DIR__ . '/footer.php'; ?>