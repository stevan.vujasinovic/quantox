<html>
<head>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300' rel='stylesheet' type='text/css'>
    <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.2.0.min.js"></script>
    <script src="js/event_counter.js"></script>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
<ul class="menu">
    <li class="menu-item"><a href="index.php?start">Home</a></li>
    <li class="menu-item"><a href="index.php?get_data&format=json">Get Statistic (JSON)</a></li>
    <li class="menu-item"><a href="index.php?get_data&format=xml">Get Statistic (XML)</a></li>
    <li class="menu-item"><a href="index.php?get_data&format=csv">Get Statistic (CSV)</a></li>
    <li class="menu-item"><a href="index.php?event_page">Create event</a></li>
</ul>
<div class="container">