

$(document).ready(function() {
    $('#event_counter_form').submit(function(event) {
        event.preventDefault();
        var country = $('select[name="country"]').val();
        var event = $('select[name="event"]').val();

        if (!country || !event) {
           return false;
        }

        $.post('index.php?start_counter', {data: JSON.stringify({ 'country': country, 'event' : event})}, function (res) {
            alert(res);
        });
    });
})