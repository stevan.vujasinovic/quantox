<?php

require __DIR__ . '/bootstrap/autoload.php';

use Core\Http\Dispatcher;

$routes = [
    'GET' => [
        'start' => ['EventCounterController' => 'index'],
        'event_page' => ['EventCounterController' => 'eventPage'],
        'get_data' => ['EventCounterController' => 'getData'],
    ],
    'POST' => [
        'start_counter' => ['EventCounterController' => 'addEventCounter'],
    ]
];
Dispatcher::handleRequest($routes);
