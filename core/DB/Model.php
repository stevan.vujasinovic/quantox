<?php
/**
 * Created by PhpStorm.
 * User: stevan
 * Date: 5.7.18.
 * Time: 16.02
 */

namespace Core\DB;

use Config\Config;

class Model
{
    private $connection;
    private $data = [];
    protected $table = 'events_counter';
    protected $primary_key = 'id';
    private $where = '';
    private $params = [];

    /**
     * Model constructor.
     */
    public function __construct()
    {
        $config = new Config();
        $db_host = $config->getDBHost();
        $db_user = $config->getDBUser();
        $db_password = $config->getDBPassword();
        $db_name = $config->getDBName();

        try {
            $this->connection = new \PDO("mysql:host=$db_host;dbname=$db_name", $db_user, $db_password);
            $this->connection->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        } catch (\PDOException $e) {
            exit('Model ERROR: ' . $e->getMessage());
        }

    }

    /**
     *
     */
    public function saveOrUpdate()
    {
        $params = '';
        $values = '';
        foreach ($this->data as $field => $value) {
            $params .= "$field, ";
            $values .= "'$value', ";
        }
        $params = substr($params, 0, -2);
        $values = substr($values, 0, -2);

        $sql = "INSERT INTO $this->table ($params) VALUES ($values) ON DUPLICATE KEY UPDATE counter = counter + 1;";

        try {

            $this->connection->exec($sql);
        } catch (\PDOException $e) {
            echo $sql . $e->getMessage();
        }

        return 'SUCCESS';
        $this->connection = null;
    }


    /**
     * @param $name
     * @param $value
     */
    public function __set($name, $value)
    {
        $this->data[$name] = $value;
    }

    /**
     * @return $this|mixed
     */
    public function get()
    {
        $sql = "SELECT * FROM $this->table $this->where";
        try {
            $stmt = $this->connection->prepare($sql);
            foreach($this->params as $placeholder => $param) {
                $stmt->bindParam($placeholder, $param);
            }
            $stmt->execute();
            return $stmt->fetch(\PDO::FETCH_OBJ);
        } catch (\PDOException $e) {
            echo $sql . $e->getMessage();
        }

        $this->connection = null;
        return $this;
    }

    /**
     * @return $this|array
     */
    public function all($fields='*')
    {
        $sql = "SELECT $fields FROM $this->table  $this->where";

        try {
            $stmt = $this->connection->prepare($sql);

            foreach($this->params as $placeholder => $param) {
                $stmt->bindParam($placeholder, $param);
            }
            $stmt->execute();
            return $stmt->fetchAll(\PDO::FETCH_ASSOC);
        } catch (\PDOException $e) {
            echo $sql . $e->getMessage();
        }

        $this->connection = null;
        return $this;
    }

    /**
     * @param $field
     * @param $operator
     * @param $value
     * @return $this
     */
    public function where($conditions)
    {
        $this->where = "WHERE ";
        foreach ($conditions as $condition)
        {
            $this->where .= "$condition[0] $condition[1] :$condition[1] AND ";
            $placeholder = ':' . $condition[1];
            $this->params[$placeholder] = $condition[2];
        }
        $this->where = substr($this->where, 0, -4);

        return $this;
    }

    public function raw($sql)
    {

        try {
            $stmt = $this->connection->prepare($sql);
            $stmt->execute();
            return $stmt->fetchAll(\PDO::FETCH_ASSOC);
        } catch (\PDOException $e) {
            echo $sql . $e->getMessage();
        }
    }


}