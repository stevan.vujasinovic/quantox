-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.7.22-0ubuntu0.16.04.1 - (Ubuntu)
-- Server OS:                    Linux
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for quantox_testing
CREATE DATABASE IF NOT EXISTS `quantox_testing` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;
USE `quantox_testing`;

-- Dumping structure for table quantox_testing.countries
CREATE TABLE IF NOT EXISTS `countries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `country_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  KEY `country_id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table quantox_testing.countries: ~10 rows (approximately)
DELETE FROM `countries`;
/*!40000 ALTER TABLE `countries` DISABLE KEYS */;
INSERT INTO `countries` (`id`, `country_name`, `country_code`) VALUES
	(1, 'United of States', 'US'),
	(2, 'Canada', 'CA'),
	(3, 'Germany', 'DE'),
	(4, 'American Samoa', 'AS'),
	(5, 'Andorra', 'AD'),
	(6, 'Angola', 'AO'),
	(7, 'Austria', 'AT'),
	(8, 'Bahamas', 'BS'),
	(9, 'Barbados', 'BB'),
	(10, 'Belarus', 'BY');
/*!40000 ALTER TABLE `countries` ENABLE KEYS */;

-- Dumping structure for table quantox_testing.events
CREATE TABLE IF NOT EXISTS `events` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `event_name` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table quantox_testing.events: ~2 rows (approximately)
DELETE FROM `events`;
/*!40000 ALTER TABLE `events` DISABLE KEYS */;
INSERT INTO `events` (`id`, `event_name`) VALUES
	(1, 'clicks'),
	(2, 'views'),
	(3, 'plays');
/*!40000 ALTER TABLE `events` ENABLE KEYS */;

-- Dumping structure for table quantox_testing.events_counter
CREATE TABLE IF NOT EXISTS `events_counter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `country_id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  `counter` bigint(20) NOT NULL,
  UNIQUE KEY `Index 4` (`event_id`,`country_id`),
  KEY `id` (`id`),
  KEY `FK_events_counter_countries` (`country_id`),
  CONSTRAINT `FK_events_counter_countries` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`),
  CONSTRAINT `FK_events_counter_events` FOREIGN KEY (`event_id`) REFERENCES `events` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table quantox_testing.events_counter: ~4 rows (approximately)
DELETE FROM `events_counter`;
/*!40000 ALTER TABLE `events_counter` DISABLE KEYS */;
/*!40000 ALTER TABLE `events_counter` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
